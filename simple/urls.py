"""simple URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView




urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url('^api/', include('api.urls')),

     url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
     url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
     url(r'^foo/$', TemplateView.as_view(template_name='base.html')),

    url(r'^accounts/',include('allauth.urls')),
    url(r'^accounts/profile/', 'profile.views.profile', name="userprofile"),
     url(r'^prf/', include('profile.urls')),
    url(r'^posts/', include('posts.urls')),
    url(r'^$', 'userpost.views.alpha_home', name='home'),

    url(r'^user-data-ops/', include('userpost.urls')),
    
    url(r'^search/', include('haystack.urls')),

    #url(r'^business/', 'posts.views.business', name='businessUrl'),
     url(r'^rest-auth/', include('rest_auth.urls')),
     url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),


]

