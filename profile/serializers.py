from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer

from .models import MyProfile, AnAvatar

from django.contrib.auth.models import User

from versatileimagefield.serializers import VersatileImageFieldSerializer


class AnAvatarSerializer(serializers.ModelSerializer):
	image = VersatileImageFieldSerializer(sizes='headshot', required=False)	
	class Meta:
		model = AnAvatar
		fields= ('image',)


class UserProfileSerializer(serializers.ModelSerializer):
	avatar = AnAvatarSerializer()

	class Meta:
		model = MyProfile
		fields = (				
				'gender',
				'birth_date',
				'about_me',
				'user_type',
				'city',
				'phone',
				'receives_new_posting_notices',
				'receives_newsletter',
				'fb_verified',
				'tw_verified',
				'ad_placement_units',
				'feature_ad_placement_units',
				'avatar'
				
			)




# class UserSerializer(UserDetailsSerializer):

#     userprofile = UserProfileSerializer(partial=True)

#     class Meta(UserDetailsSerializer.Meta):
#         fields = UserDetailsSerializer.Meta.fields + ('userprofile',)

#     def update(self, instance, validated_data):
#         profile_data = validated_data.pop('userprofile', {})
#         #company_name = profile_data.get('company_name')

#         instance = super(UserSerializer, self).update(instance, validated_data)

#         # get and update user profile
#         profile = instance.userprofile
#         if profile_data and company_name:
#             profile.company_name = company_name
#             profile.save()
#         return instance

class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(partial=True,)
    class Meta:
        model = User
        fields = ('url', 'username', 'email','first_name', 'last_name','profile')
	
	def create(self, validated_data, ):
		profile_data=validated_data.pop('profile')
		user = User.objects.create(
		    username=validated_data['username'],
		    email=validated_data['email'],
		    first_name=validated_data['first_name'],
		    last_name=validated_data['last_name']
		    )
		user.set_password(validated_data['password'])
		user.save()
		MyProfile.objects.create(user=user, **profile_data)
		return user