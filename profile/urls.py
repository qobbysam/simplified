from django.conf.urls import include, url

from profile import views

urlpatterns = [
    #url(r'^signin/', views.signin, name ="signin"),
    # url(r'^signout/',views.signout, name="signout" ),

    #url(r'^signout/','django.contrib.auth.views.logout',name='signout',kwargs={'next_page': '/'}),
    #url(r'^signup/', views.signup, name = "signup"),
    #url(r'^signup-success/', views.signup_success),

    url(r'^profile/(?P<pk>\d+)/$', views.profile, name ="myprofiledetail"),
    #url(r'^profile/', views.profile, name ="myprofiledetail"),

    #url(r'^reset-password/',views.reset_password,name = "resetpassword"),
    url(r'^update_info/', views.update_info, name = "updateinfo")
        ]