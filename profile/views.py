from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.core.context_processors import csrf

from django.contrib.auth.forms import UserCreationForm

from .models import MyProfile
from posts.models import AdImg

from .forms import UpdateProfileForm, UserUpdateForm 
from django.contrib.auth.models import User





@login_required
def profile(request):
	user_instance = User.objects.get(user=request.user)
	my_profile_instance = MyProfile.objects.get(user=user_instance)

	if request.method == "POST":
		form1 = UserUpdateForm(request.POST, instance=user_instance)
		form2 = UpdateProfileForm(request.POST, request.Files, instance=my_profile_instance)

		if form1.is_valid() and form2.is_valid():
			a =form1.save()
			b =form2.save()

			return redirect('userpost_settings')
			

	else:
		form1 = UserUpdateForm(instance=user_instance)
		form2 = UpdateProfileForm(instance=my_profile_instance)



	

	args = {}
	args.update(csrf(request))
	args['form1'] = form1
	args['form2'] = form2
	
	



	return render(request, 'userpost/setting.html',args,context_instance=RequestContext(request))

def profile_detail(request):
	pass

def update_info(request):
	pass