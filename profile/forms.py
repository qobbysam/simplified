from django import forms

from .models import MyProfile

from django.contrib.auth.models import User


class UserUpdateForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'email')
		labels = {
			'first_name':' Prenom',
			'last_name': 'Nom de famile',
			'email': 'Email',
		}


class UpdateProfileForm(forms.ModelForm):
	#avatar = forms.FileField(required = False)

	class Meta:
		model = MyProfile
		fields = ('gender',
				'birth_date',
				'about_me',
				'user_type',
				'city',
				'phone',
				'receives_new_posting_notices',
				'receives_newsletter')

		labels = {
		'birth_date':'Date de naissance',
		'city':'Ville',
		'phone': 'Numero de Telephone'
		}




