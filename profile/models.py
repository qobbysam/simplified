from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User

from versatileimagefield.fields import VersatileImageField
from versatileimagefield.placeholder import OnDiscPlaceholderImage

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
import os

# Create your models here.



class AnAvatar(models.Model):
	image = VersatileImageField(
		'Image',
		upload_to='/media/avatars',
		blank=True,
		placeholder_image = OnDiscPlaceholderImage(
			path = os.path.join(settings.MEDIA_ROOT, 'placeholder', 'avatar_placeholder.png')),
		)
def getdefault():
	return AnAvatar.objects.get(id=1)

class MyProfile(models.Model):
	""" Default profile """
	GENDER_CHOICES = (
	(1, _('Male')),
	(2, _('Female')),
	)

	BUSINESS_CHOICES =(('p','Personal'),('b','Business'))

	user = models.OneToOneField(User,
		unique=True,verbose_name=_('user'),
		related_name='profile')

	gender = models.PositiveSmallIntegerField(_('gender'),
	          choices=GENDER_CHOICES,
	          blank=True,
	          null=True)

	birth_date = models.DateField(_('birth date'), blank=True, null=True)
	about_me = models.TextField(_('about me'), blank=True)
	user_type = models.CharField(_('usertype'),choices=BUSINESS_CHOICES,default=BUSINESS_CHOICES[0],max_length=15,null=True)
	#favourites = models.ManyToManyField(Ad, related_name='my_favorites', null=True, blank=True)
	city = models.CharField(_('city'),max_length=100, blank=True, default='', null= True)
	phone = models.IntegerField(_('phone_number'),blank=True,null=True)
	receives_new_posting_notices = models.BooleanField(default=False)
	receives_newsletter = models.BooleanField(default=False)

	fb_verified = models.BooleanField(default=False)
	tw_verified = models.BooleanField(default=False)

	ad_placement_units = models.IntegerField(default = 500)
	feature_ad_placement_units = models.IntegerField(default=0)

	avatar = models.ForeignKey(AnAvatar, related_name="avatarimg", default=getdefault)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        MyProfile.objects.create(user=instance)
post_save.connect(create_user_profile, sender=User)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)






















#Security