# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MyProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='gender', choices=[(1, 'Male'), (2, 'Female')])),
                ('birth_date', models.DateField(null=True, verbose_name='birth date', blank=True)),
                ('about_me', models.TextField(verbose_name='about me', blank=True)),
                ('user_type', models.CharField(default=(b'p', b'Personal'), max_length=15, null=True, verbose_name='usertype', choices=[(b'p', b'Personal'), (b'b', b'Business')])),
                ('city', models.CharField(default=b'', max_length=100, null=True, verbose_name='city', blank=True)),
                ('phone', models.IntegerField(null=True, verbose_name='phone_number', blank=True)),
                ('receives_new_posting_notices', models.BooleanField(default=False)),
                ('receives_newsletter', models.BooleanField(default=False)),
                ('fb_verified', models.BooleanField(default=False)),
                ('tw_verified', models.BooleanField(default=False)),
                ('ad_placement_units', models.IntegerField(default=500)),
                ('feature_ad_placement_units', models.IntegerField(default=0)),
                ('avatar', versatileimagefield.fields.VersatileImageField(upload_to=b'/media/avatars', verbose_name=b'Image', blank=True)),
                ('user', models.OneToOneField(related_name='profile', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
