# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0002_auto_20151231_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myprofile',
            name='avatar',
            field=models.ForeignKey(related_name='avatarimg', default=1, to='profile.AnAvatar'),
        ),
    ]
