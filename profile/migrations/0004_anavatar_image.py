# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_auto_20151231_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='anavatar',
            name='image',
            field=versatileimagefield.fields.VersatileImageField(upload_to=b'/media/avatars', verbose_name=b'Image', blank=True),
        ),
    ]
