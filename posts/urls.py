from django.conf.urls import include, url

from posts import views

urlpatterns = [
    url(r'^$', views.home, name ="home"),
    url(r'^category/(?P<category>.*)', views.category, ),
    #url(r'^create/', views.create_ad, name="createad"),
    url(r'^ad-post/(?P<pk>\d+)/$', views.ad_page,name='adpage'),
    url(r'^ad-preview/(?P<pk>\d+)/$', views.preview_ad,name='adpreview'),
    # url(r'^edit-ad/',),
    # url(r'^search/',),
    #url(r'^post-search/', search_view_factory(view_class = SearchView, form_class = CustomSearchForm), name='haystack_search'),



    ]