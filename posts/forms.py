from django import forms
from haystack.forms import SearchForm
import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field

from .models import AdImg, AdPost, AllowedList, Category, Fields, AnImage


class AdPostForm(forms.ModelForm):
	class Meta:
		model = AdPost
		fields = ('category', 'title', 'city' )
		labels = {
			'category':'Categorie',
			'title': 'Titre',
			'city': 'Ville'
		}

class FieldsForm(forms.ModelForm):
	class Meta:
		model = Fields
		fields = ('price', 
			'delivery_options',
			'phone_number',
			'more_detail')
		labels = {
			'price': 'Prix',
			'delivery_options':'Option de delivery',
			'phone_number':'Numero de Telephone',
			'more_detail': 'Detail du Produit',
		}




class ImgUpdate(forms.Form):
	image = forms.ImageField(label= "Image", required=False)

class AnImageForm1(forms.Form):
	img_1 = forms.ImageField(label = "Your First Image", required=False)
	img_2 = forms.ImageField(label="Your Second Image", required=False)
	img_3 = forms.ImageField(label="Your Third Image", required=False)
	img_4 = forms.ImageField(label="Your Fourth Image", required=False)
	img_5 = forms.ImageField(label="Your Fifth Image", required=False)










# class AnImageForm2(forms.ModelForm):

# 	class Meta:
# 		model=AnImage
# 		exclude=()
# class AnImageForm3(forms.ModelForm):

# 	class Meta:
# 		model=AnImage
# 		exclude=()
# class AnImageForm4(forms.ModelForm):

# 	class Meta:
# 		model=AnImage
# 		exclude=()
# class AnImageForm5(forms.ModelForm):

# 	class Meta:
# 		model=AnImage
# 		exclude=()


# class AdvancedSearchForm(SearchForm):
# 	city = forms.CharField(required=False)
# 	price_min= forms.PositiveIntegerField(required=False)
# 	price_max= forms.PositiveIntegerField(required=False)
# 	start_date = forms.DateField(required=False )
# 	end_date = forms.DateField(required=False)

# 	def search(self):
# 		# First, store the SearchQuerySet received from other processing.
# 		sqs = super(AdvancedSearchForm, self).search()

# 		if not self.is_valid():
# 			return self.no_query_found()

# 		if self.cleaned_data['start_date']:

# 			sqs=sqs.filter(ad__date_on_creation__lte=self.cleaned_data['start_date'])
# 		if self.cleaned_data['end_date']:
# 			sqs=sqs.filter(ad__date_on_creation__gte=self.cleaned_data['end_date'])

# 		if self.cleaned_data['price_min']:
# 			sqs = sqs.filter(ad__ad_fields__price__lte=self.cleaned_data['price_min'])

# 		if self.cleaned_data['price_max']:
# 			sqs = sqs.filter(ad__ad_fields__price__gte=self.cleaned_data['price_max'])


# 		if self.cleaned_data['city']:
# 			sqs = sqs.filter(ad__city=self.cleaned_data['city'])


# 		return sqs

