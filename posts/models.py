from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from versatileimagefield.fields import VersatileImageField
from versatileimagefield.placeholder import OnDiscPlaceholderImage
from simple import settings
import os
from PIL import Image


# Create your models here.

upload_to_path = settings.ADIMAGELOCATION

class Fields(models.Model):
	DELIVIRY_CHOICES =(('p','Pickup'),('d','Delivery'),('m','Meetup'))
	price = models.PositiveIntegerField(null=True)
	delivery_options = models.CharField(choices=DELIVIRY_CHOICES, default= DELIVIRY_CHOICES[0], max_length=11)
	phone_number = models.PositiveIntegerField(null=True)
	more_detail = models.TextField(null=True)
	


	def __unicode__(self):
		return str(self.phone_number)


class Category(models.Model):
	cat_name = models.CharField(max_length=50)
	slug = models.SlugField()
	description = models.TextField()
	sort_order = models.PositiveIntegerField(default=0)
	sortby_fields = models.CharField(max_length=200,help_text=_(u'A comma separated list of field names that should show up as sorting options.'),blank=True)
	parent = models.ForeignKey('self', blank=True, null=True, related_name='child')

	def __unicode__(self):
		return self.cat_name





class AdPost(models.Model):
	title = models.CharField(max_length= 200)
	#owner = models.ForeignKey(User, related_name = 'adowner')
	city = models.CharField(max_length = 100)
	category = models.ForeignKey(Category,related_name='adcategory')
	ad_fields= models.ForeignKey(Fields, related_name='adfields')
	date_on_creation = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.title


class AnImage(models.Model):
	image = VersatileImageField('Image',
		upload_to=upload_to_path,
		blank=True, 
		placeholder_image = OnDiscPlaceholderImage(
			path = os.path.join(settings.MEDIA_ROOT, 'placeholder', 'no_image400.jpg')),
		)

def getdefault():
	return AnImage.objects.get(id=1)

class AllowedList(models.Model):

	img_1 = models.ForeignKey(AnImage, related_name='imagesinlist1', default=getdefault)
	img_2 = models.ForeignKey(AnImage, related_name='imagesinlist2', default=getdefault)
	img_3 = models.ForeignKey(AnImage, related_name='imagesinlist3', default=getdefault)
	img_4 = models.ForeignKey(AnImage, related_name='imagesinlist4', default=getdefault)
	img_5 = models.ForeignKey(AnImage, related_name='imagesinlist5', default=getdefault)



class AdImg(models.Model):
	ad = models.ForeignKey(AdPost, related_name='adpost')
	imgs = models.ForeignKey(AllowedList, related_name='adimgowner')
	owner = models.ForeignKey(User, related_name='adimgowner')
	allow_world = models.BooleanField(default=True)
	active = models.BooleanField(default=True)
	approved = models.BooleanField(default=False)

	def __unicode__(self):
		return self.ad.title





