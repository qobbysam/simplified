# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AdImg',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('allow_world', models.BooleanField(default=True)),
                ('active', models.BooleanField(default=True)),
                ('approved', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='AdPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('city', models.CharField(max_length=100)),
                ('date_on_creation', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='AllowedList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='AnImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', versatileimagefield.fields.VersatileImageField(upload_to=b'adimgs/', verbose_name=b'Image', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cat_name', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField()),
                ('sort_order', models.PositiveIntegerField(default=0)),
                ('sortby_fields', models.CharField(help_text='A comma separated list of field names that should show up as sorting options.', max_length=200, blank=True)),
                ('parent', models.ForeignKey(related_name='child', blank=True, to='posts.Category', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Fields',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.PositiveIntegerField(null=True)),
                ('delivery_options', models.CharField(default=(b'p', b'Pickup'), max_length=11, choices=[(b'p', b'Pickup'), (b'd', b'Delivery'), (b'm', b'Meetup')])),
                ('phone_number', models.PositiveIntegerField(null=True)),
                ('more_detail', models.TextField(null=True)),
            ],
        ),
        migrations.AddField(
            model_name='allowedlist',
            name='img_1',
            field=models.ForeignKey(related_name='imagesinlist1', to='posts.AnImage'),
        ),
        migrations.AddField(
            model_name='allowedlist',
            name='img_2',
            field=models.ForeignKey(related_name='imagesinlist2', to='posts.AnImage'),
        ),
        migrations.AddField(
            model_name='allowedlist',
            name='img_3',
            field=models.ForeignKey(related_name='imagesinlist3', to='posts.AnImage'),
        ),
        migrations.AddField(
            model_name='allowedlist',
            name='img_4',
            field=models.ForeignKey(related_name='imagesinlist4', to='posts.AnImage'),
        ),
        migrations.AddField(
            model_name='allowedlist',
            name='img_5',
            field=models.ForeignKey(related_name='imagesinlist5', to='posts.AnImage'),
        ),
        migrations.AddField(
            model_name='adpost',
            name='ad_fields',
            field=models.ForeignKey(related_name='adfields', to='posts.Fields'),
        ),
        migrations.AddField(
            model_name='adpost',
            name='category',
            field=models.ForeignKey(related_name='adcategory', to='posts.Category'),
        ),
        migrations.AddField(
            model_name='adimg',
            name='ad',
            field=models.ForeignKey(related_name='adpost', to='posts.AdPost'),
        ),
        migrations.AddField(
            model_name='adimg',
            name='imgs',
            field=models.ForeignKey(related_name='adimgowner', to='posts.AllowedList'),
        ),
        migrations.AddField(
            model_name='adimg',
            name='owner',
            field=models.ForeignKey(related_name='adimgowner', to=settings.AUTH_USER_MODEL),
        ),
    ]
