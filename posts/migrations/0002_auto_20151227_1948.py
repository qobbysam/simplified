# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import posts.models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='allowedlist',
            name='img_1',
            field=models.ForeignKey(related_name='imagesinlist1', default=posts.models.getdefault, to='posts.AnImage'),
        ),
        migrations.AlterField(
            model_name='allowedlist',
            name='img_2',
            field=models.ForeignKey(related_name='imagesinlist2', default=posts.models.getdefault, to='posts.AnImage'),
        ),
        migrations.AlterField(
            model_name='allowedlist',
            name='img_3',
            field=models.ForeignKey(related_name='imagesinlist3', default=posts.models.getdefault, to='posts.AnImage'),
        ),
        migrations.AlterField(
            model_name='allowedlist',
            name='img_4',
            field=models.ForeignKey(related_name='imagesinlist4', default=posts.models.getdefault, to='posts.AnImage'),
        ),
        migrations.AlterField(
            model_name='allowedlist',
            name='img_5',
            field=models.ForeignKey(related_name='imagesinlist5', default=posts.models.getdefault, to='posts.AnImage'),
        ),
    ]
