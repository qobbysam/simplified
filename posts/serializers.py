from rest_framework import serializers
from django.contrib.auth.models import User
from versatileimagefield.serializers import VersatileImageFieldSerializer

from .models import AdImg, AdPost, AllowedList, Category, Fields, AnImage 

class AnImageSerializer(serializers.ModelSerializer):
	image = VersatileImageFieldSerializer(sizes='ad_reps', required=False)	
	class Meta:
		model = AnImage
		fields= ('image',)


class AllowedListSerializer(serializers.ModelSerializer):
	img_1 = AnImageSerializer()
	img_2 = AnImageSerializer()
	img_3 = AnImageSerializer()
	img_4 = AnImageSerializer()
	img_5 = AnImageSerializer()

	class Meta:
		model= AllowedList
		fields = ('img_1', 'img_2', 'img_3', 'img_4', 'img_5',)

	def update(self,validated_data,instance):
		instance.img_1 = validated_data.get('img_1', instance.img_1)
		instance.img_2 = validated_data.get('img_2', instance.img_2)
		instance.img_3 = validated_data.get('img_3', instance.img_3)
		instance.img_4 = validated_data.get('img_4', instance.img_4)
		instance.img_5 = validated_data.get('img_5', instance.img_5)
		instance.save()

		return instance



class FieldSerializer(serializers.ModelSerializer):

	class Meta:
		model = Fields
		fields = ('price', 'delivery_options','phone_number','more_detail',)

class CreatefieldsSerializer(serializers.Serializer):
	DELIVIRY_CHOICES =(('p','Pickup'),('d','Delivery'),('m','Meetup'))
	price = serializers.IntegerField()
	phone_number= serializers.IntegerField()
	delivery_options=serializers.ChoiceField(choices = DELIVIRY_CHOICES)
	more_detail= serializers.CharField(style={'base_template': 'textarea.html'})


class createAdpostSerializer(serializers.Serializer):
	title=serializers.CharField()
	#owner=serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())
	city=serializers.CharField()
	category=serializers.PrimaryKeyRelatedField(many=False, queryset= Category.objects.all())
	ad_fields=CreatefieldsSerializer()
class AdPostSerializer(serializers.ModelSerializer):
	#category =  serializers.StringRelatedField(many=False)
	category=serializers.PrimaryKeyRelatedField(many=False, queryset= Category.objects.all())
	ad_fields = FieldSerializer()

	class Meta:
		model = AdPost
		fields = (
				'title',			
				'city',
				'category',
				'ad_fields',
			
				'id',
			)




class AdImgPartOne(serializers.ModelSerializer):
	ad = AdPostSerializer()
	class Meta:
		model = AdImg
		fields = ('ad','id','allow_world',)

	def create(self,validated_data):
		ad_data = validated_data['ad']
		field_data = validated_data['ad_fields']

		field_instance = Fields.objects.create(**fields_data)
		Adpost_instance = AdPost.objects.create(ad_fields=field_instance, 
			category=ad_data['category'],
			title= ad_data['title'],
			city= ad_data['city'])
		imgs = AllowedList.objects.get(id=1)

		new_ad_imgs = AdImg.objects.create(ad=Adpost_instance,imgs=imgs, owner=request.user)
		return new_ad_imgs




class CreateAdImgSerializer(serializers.ModelSerializer):

	ad = AdPostSerializer(partial=True)
	#owner=serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())
	imgs = AllowedListSerializer(partial=True, required=False) 
	class Meta:
		model = AdImg
		fields =('ad', 'imgs','id','allow_world',)


	def create(self, validated_data,):
		ad_data = validated_data['ad']
		imgs_data = validated_data['imgs']
		fields_data = ad_data['ad_fields']
		field_instance  = Fields.objects.create(**fields_data)
		Adpost_instance = AdPost.objects.create(ad_fields=field_instance,
			#owner = ad_data['owner'],
			allow_world = ad_data['allow_world'],
			category= ad_data['category'],
			title = ad_data['title'],
			city = ad_data['city'])
		
		img_instance = AllowedList.objects.create()

		a = AdImg.objects.create(ad=Adpost_instance, imgs=img_instance, owner=request.user)

		return a


class CategorySerializer(serializers.ModelSerializer):

	class Meta:
		model = Category
		fields = (
			'cat_name',
			'slug',
			'description',
			'sort_order',
			'sortby_fields',
			'parent',
			'id')





class AdImgSerializer(serializers.ModelSerializer):
	ad = AdPostSerializer()
	imgs = AllowedListSerializer(partial = True)
	class Meta:
		model = AdImg
		fields = ('ad','imgs','id')
	
	def create(self,validated_data):
		ad_data = validated_data.pop('ad')
		field_data = ad_data.pop('ad_fields')

		#if ad_data.is_valid():
		ad_field_data = Fields.objects.create(**field_data)
		ad_field_data.save()
		ad_data_data = AdPost.objects.create(ad_fields=ad_field_data, **ad_data)
		ad_data_data.save()

		ap = AllowedList.objects.create()

		lu = AdImg.objects.create(ad=ad_data_data, imgs=ap, owner= request.user)
		
		return lu.id



class CreatefieldsSerializer(serializers.Serializer):
	DELIVIRY_CHOICES =(('p','Pickup'),('d','Delivery'),('m','Meetup'))
	price = serializers.IntegerField()
	phone_number= serializers.IntegerField()
	delivery_options=serializers.ChoiceField(choices = DELIVIRY_CHOICES)
	allow_world= serializers.BooleanField(default=True)
	more_detail= serializers.CharField(style={'base_template': 'textarea.html'})



class createAdpostSerializer(serializers.Serializer):
	title=serializers.CharField()
	#owner=serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())
	city=serializers.CharField()
	category=serializers.PrimaryKeyRelatedField(many=False, queryset= Category.objects.all())
	ad_fields=CreatefieldsSerializer()










