from .models import Category, AdImg, AdPost, AllowedList, Fields, AnImage
from .forms import AdPostForm, FieldsForm


def getimgsurl(pk):
	urlList = []
	message =""
	try:
		alobj = AllowedList.objects.get(pk=pk)		
		urlList.append(alobj.img_1.image.thumbnail['400x400'].url)		
		urlList.append(alobj.img_2.image.thumbnail['400x400'].url)
		urlList.append(alobj.img_3.image.thumbnail['400x400'].url)
		urlList.append(alobj.img_4.image.thumbnail['400x400'].url)
		urlList.append(alobj.img_5.image.thumbnail['400x400'].url)
		message = "list get complete"
	except Exception:
		message = "obj does not exist"

	return urlList

def getAllowedList(pk):
	message = ""
	pkofallowed=0 
	try:
		AdImgobj = AdImg.objects.get(pk=pk)
		pkofallowed = AdImgobj.imgs.id
		message = "object found"
	except Exception:
		message=" object does not exist"

	# return {message:pkofallowed}
	return pkofallowed

def getAdPostpk(pk):
	message = ""
	pkofadpost=0 
	try:
		AdImgobj = AdImg.objects.get(pk=pk)
		pkofpost = AdImgobj.ad.id
		message = "object found"
	except Exception:
		message=" object does not exist"

	# return {message:pkofallowed}
	print message
	return pkofpost

def deleteImg(pk):
	message =""
	try:
		AnImage.objects.filter(pk=pk).delete()
		message = "image was deleted"
	except Exception:
		message = "image does not exist"

	return message

def getAdImg(pk):
	message = ""
	ad = ""
	try:
		ad=AdImg.objects.get(pk=pk)
		return {message:ad}
	except Exception:
		message = "no obj not found"
		return{message:ad}

def getFieldspk(pk):
	message = ""
	pk_of_field=0 
	try:
		Adpostobj = AdPost.objects.get(pk=pk)
		pk_of_field = Adpostobj.ad_fields.id
		message = "object found"
	except Exception:
		message=" object does not exist"

	# return {message:pkofallowed}
	print message
	return pk_of_field



def handlemultipleimages(files,*args, **kwargs):
	count = 0
	listpk = []
	instance_id = 1
	for files in files:
		ps = AnImage.object.create(image=files)
		count = count+1
		listpk.append(ps.id)

	allist_instance = AllowedList.objects.create()	
	if count == 5:
		allist_instance.update(img_1=AnImage.objects.get(pk=listpk[0]),
								img_2=AnImage.objects.get(pk=listpk[1]),
								img_3=AnImage.objects.get(pk=listpk[2]),
								img_4=AnImage.objects.get(pk=listpk[3]),
								img_5=AnImage.objects.get(pk=listpk[4]),)
		instance_id = allist_instance
	if count == 4:
		allist_instance.update(img_1=AnImage.objects.get(pk=listpk[0]),
								img_2=AnImage.objects.get(pk=listpk[1]),
								img_3=AnImage.objects.get(pk=listpk[2]),
								img_4=AnImage.objects.get(pk=listpk[3]),)
		instance_id = allist_instance
	if count == 3:
		allist_instance.update(img_1=AnImage.objects.get(pk=listpk[0]),
								img_2=AnImage.objects.get(pk=listpk[1]),
								img_3=AnImage.objects.get(pk=listpk[2]),)
		instance_id = allist_instance

	if count == 2:
		allist_instance.update(img_1=AnImage.objects.get(pk=listpk[0]),
								img_2=AnImage.objects.get(pk=listpk[1]),)
		instance_id = allist_instance
	if count == 1:
		allist_instance.update(img_1=AnImage.objects.get(pk=listpk[0]),
			)
		instance_id = allist_instance

	if count == 0:
		instance_id = allist_instance	

	return instance_id


