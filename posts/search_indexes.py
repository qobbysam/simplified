import datetime
from haystack import indexes
from .models import AdImg, AdPost







class AdImgIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	#title = indexes.CharField(model_attr='title', faceted=True)
	#date_on_creation = indexes.DateTimeField(model_attr='date_on_creation')
	ad = indexes.CharField(model_attr='ad', faceted=True)

	def get_model(self):
		return AdImg

	# def index_queryset(self, using=None):
	# 	"""Used when the entire index for model is updated."""
	# 	return self.get_model().objects.filter(ad__date_on_creation__lte=datetime.datetime.now())

	# def prepare_ad(self, obj):
	# 	return obj.ad.title

