from django.contrib import admin
from .models import AdImg, AdPost, AllowedList, Category, Fields, AnImage
# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
	pass

class AdImgAdmin(admin.ModelAdmin):
	pass

class AdPostAdmin(admin.ModelAdmin):
	pass

class AllowedListAdmin(admin.ModelAdmin):
	pass
class FieldsAdmin(admin.ModelAdmin):
	pass

class AnImageAdmin(admin.ModelAdmin):
	pass
admin.site.register(Category,CategoryAdmin)
admin.site.register(AdPost, AdPostAdmin)
admin.site.register(AllowedList, AllowedListAdmin)
admin.site.register(Fields, FieldsAdmin)
admin.site.register(AdImg, AdImgAdmin)
admin.site.register(AnImage, AnImageAdmin)
