from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.core.context_processors import csrf

from .models import Category, AdImg, AdPost, AllowedList, Fields, AnImage

from utils import getimgsurl, getAdImg, deleteImg, getAllowedList, getAdPostpk, getFieldspk, getimgsurl

#from django.db.models import Q

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .forms import  AdPostForm, FieldsForm, AnImageForm1 

# Create your views here.

def home(request):
	cats = Category.objects.all()
	args = {}
	args.update(csrf(request))
	args['cats'] = cats
	return render_to_response('home.html', args,context_instance=RequestContext(request))


def category(request,category):
	key = request.GET.get('category','')

	
	if key is not None:
		query = AdImg.objects.filter(ad__category__cat_name=key).filter(active = True)

		paginator = Paginator(query, 5) # Show 25 contacts per page

		page = request.GET.get('page')
		try:
			ns = paginator.page(page)
		except PageNotAnInteger:
		# If page is not an integer, deliver first page.
			ns= paginator.page(1)
		except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
			ns = paginator.page(paginator.num_pages)
	# get any current GET parameters without the page modifier
	extra_param = request.GET.copy()
	if 'page' in  extra_param.keys():
		del  extra_param['page']

		#length = len(object_list)

	args = {}
	args.update(csrf(request))

	args['ads'] = query
	args['ns'] = ns
	args['params']= extra_param
	print ns
	
	return render_to_response('category.html', args,context_instance=RequestContext(request))


def search_ad(request):
	pass

def ad_page(request,pk):
	adpk = pk

	ad_detail = AdImg.objects.filter(pk=adpk)

	alpk = getAllowedList(adpk)

	print alpk
	imgs = getimgsurl(alpk)
	
	args = {}

	args['ad_detail'] = ad_detail
	args['imgs'] = imgs
	args['pk'] = adpk
	

	return render_to_response('ad_post.html', args, context_instance=RequestContext(request))


def preview_ad(request,pk):
	adimgpk = pk 
	ad_instance = AdImg.objects.get(pk=pk)

	alpk = getAllowedList(adimgpk) 

	imgs = getimgsurl(alpk)
	args = {}
	args.update(csrf(request))

	args['imgs'] = imgs
	args['post'] = ad_instance
	args['pk'] = adimgpk

	return render_to_response('preview.html', args, context_instance=RequestContext(request))




