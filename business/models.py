from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User

from versatileimagefield.fields import VersatileImageField

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

# Create your models here.

class BusinessProfile(models.model):
	about_company = models.TextField(_('about me'), blank=True)
	user = models.OneToOneField(User,
		unique=True,verbose_name=_('user'),
		related_name='businessprofile')

	city = models.CharField(_('city'),max_length=100, blank=True, default='', null= True)
	phone = models.IntegerField(_('phone_number'),blank=True,null=True)
	ad_placement_units = models.IntegerField(default = 500)
	feature_ad_placement_units = models.IntegerField(default=0)
	company_image = VersatileImageField(
		'Image',
		upload_to='/media/avatars',
		blank=True
		)

def create_business_profile(sender, instance, created, **kwargs):
    if created:
        BusinessProfile.objects.create(user=instance)
post_save.connect(create_business_profile, sender=User)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
