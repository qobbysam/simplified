from django.shortcuts import render

from rest_framework import generics, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView, ListCreateAPIView
from rest_framework.parsers import FormParser, MultiPartParser

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from profile.models import MyProfile
from profile.serializers import UserSerializer

from posts.models import AdImg, AdPost, AllowedList, Category, Fields
from posts.serializers import AdImgSerializer, AdPostSerializer, AllowedListSerializer, CreateAdImgSerializer , CategorySerializer, FieldSerializer

from django.contrib.auth.models import User
from .permissions import IsOwnerOrReadOnly, IsProfileOrReadOnly, IsAdownerOrReadOnly

from posts.utils import getimgsurl

# Create your views here.



class mytestview(APIView):
	#authentication_classes = (authentication.TokenAuthentication,)
	#permission_classes = (AllowAny)

	def get(self,request,pk,format=None):
		
		imgs = getimgsurl(pk)
		
		return Response(imgs)


class PrepareImgs(APIView):

	def get(self,request,pk,format=None):
		
		imgs = getimgsurl(pk)
		
		return Response(imgs)




class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer
	parser_classes = (MultiPartParser, FormParser,)
	#permission_classes = (IsOwnerOrReadOnly,)
	def get_permissions(self):
		return (AllowAny() if self.request.method == 'POST'
			else  IsProfileOrReadOnly()),

	def perform_update(self,serializer):
		serializer.save(profile__user=self.request.user,profile__avatar=self.request.data.get('avatar'))



#Read only for categories
class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer
	allowed_methods = ['GET']
	#permission_classes = (AllowAny)

#read only for ads
class AdPostViewSet(viewsets.ReadOnlyModelViewSet):
	queryset = AdImg.objects.all()
	serializer_class = AdImgSerializer
	#fxreallowed_methods = ['GET']



#search category
class CategorySearchList(generics.ListAPIView):
	serializer_class = AdImgSerializer


	def get_queryset(self):
		# category = self.kwargs['category']

		# return AdPost.objects.Filter(ad__category__cat_name = category)

		queryset = AdImg.objects.all()
		category = self.request.query_params.get('category', None)
		if category is not None:
			queryset = queryset.filter(ad__category__cat_name = category)
		return queryset


class CreateAdViewset(viewsets.ModelViewSet):
	serializer_class = CreateAdImgSerializer
	queryset = AdImg.objects.all()
	parser_classes = (MultiPartParser, FormParser,)

	def get_permissions(self):
		return (AllowAny() if self.request.method == 'POST'
			else  IsAdownerOrReadOnly()),





