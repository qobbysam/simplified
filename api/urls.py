

from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views


from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from .views import UserViewSet, AdPostViewSet, PrepareImgs, CategoryViewSet , CategorySearchList, CreateAdViewset, mytestview



v1_api = DefaultRouter()
v1_api.register(r'user', UserViewSet)
v1_api.register(r'adp', AdPostViewSet)
v1_api.register(r'cat', CategoryViewSet)
v1_api.register(r'cpost',CreateAdViewset)



urlpatterns = [
    
    url(r'^',include(v1_api.urls)),
    url(r'^api-auth/', views.obtain_auth_token),
    # url(r'^rest-auth/', include('rest_auth.urls')),
    # url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^api-auth-rest/', include('rest_framework.urls',namespace='rest_framework')),

    url(r'^cat-home/(?P<category>.*)$',CategorySearchList.as_view()),
    url(r'^testview/(?P<pk>[0-9]+)/$', mytestview.as_view()),
    url(r'^imgslist/(?P<pk>[0-9]+)/$', PrepareImgs.as_view()),
    #url(r'^cpost/', CreateAdViewset.as_view()),
    ]
