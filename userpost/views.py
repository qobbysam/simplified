
from profile.models import MyProfile, AnAvatar
from profile.forms import UpdateProfileForm, UserUpdateForm

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.core.context_processors import csrf
from django.contrib.auth.models import User

from posts.models import Category, AdImg, AdPost, AllowedList, Fields, AnImage
from posts.forms import AdPostForm, FieldsForm, AnImageForm1, ImgUpdate
from posts.utils import getimgsurl, getAdImg, deleteImg, getAllowedList, getAdPostpk, getFieldspk
# Create your views here.


@login_required
def profile_home(request):

	getUserAd = AdImg.objects.filter(owner=request.user)
	args={}
	args.update(csrf(request))

	args['userpostlist'] = getUserAd

	return render_to_response('profilehome.html', args, context_instance=RequestContext(request))

	
@login_required
def edit_ad(request,pk):
	adimgpk = pk
	adpostpk = getAdPostpk(adimgpk)
	listpk = getAllowedList(adimgpk)
	fieldpk = getFieldspk(adpostpk)

	adpost_instance = AdPost.objects.get(pk=adpostpk)
	fields_instance = Fields.objects.get(pk=fieldpk)
	AllowedList_instance = AllowedList.objects.get(pk=listpk)

	if request.method == "POST" :
		form1 = AdPostForm(request.POST, instance= adpost_instance)
		#form3 = ImgsForm(request.POST,request.FILES)
		form2 = FieldsForm(request.POST, instance = fields_instance)
		if form1.is_valid() and form2.is_valid():
			field  = form2.save()
			ad_post = form1.save(commit=False)
			ad_post.ad_fields = field
			ad_post.save()

			#imgs = AllowedList.objects.get(id=1)

			new_ad_imgs = AdImg.objects.filter(pk = adimgpk).update(ad=ad_post,imgs=listpk, owner=request.user)
			
			#return redirect('userpost_addimgs',pk=new_ad_imgs.id)
	else:
		
		form1 = AdPostForm(instance = adpost_instance)
		#form3 = ImgsForm()
		form2 = FieldsForm(instance = fields_instance)


	args = {}
	args.update(csrf(request))
	#args['form1'] = form1
	args['form1'] = form1
	#args['form3'] = form3
	args['form2'] = form2
	args['form3'] = ImgUpdate()
	args['adpk'] = adimgpk
	args['listimgs'] = AllowedList_instance
	args['listimgspk'] = AllowedList_instance.id
	args['img1pk'] = AllowedList_instance.img_1.id
	args['img2pk'] = AllowedList_instance.img_2.id
	args['img3pk'] = AllowedList_instance.img_3.id
	args['img4pk'] = AllowedList_instance.img_4.id
	args['img5pk'] = AllowedList_instance.img_5.id

	return render_to_response('editad.html', args, context_instance=RequestContext(request))



	

@login_required
def create_ad(request):
	if request.method == "POST" :
		form1 = AdPostForm(request.POST)
		#form3 = ImgsForm(request.POST,request.FILES)
		form2 = FieldsForm(request.POST)
		if form1.is_valid() and form2.is_valid():
			field  = form2.save()
			ad_post = form1.save(commit=False)
			ad_post.ad_fields = field
			ad_post.save()

			imgs = AllowedList.objects.get(id=1)

			new_ad_imgs = AdImg.objects.create(ad=ad_post,imgs=imgs, owner=request.user)
			
			return redirect('userpost_addimgs',pk=new_ad_imgs.id)
	else:
		
		form1 = AdPostForm()
		#form3 = ImgsForm()
		form2 = FieldsForm()
	args = {}
	args.update(csrf(request))
	#args['form1'] = form1
	args['form1'] = form1
	#args['form3'] = form3
	args['form2'] = form2


	return render_to_response('up_create.html', args, context_instance=RequestContext(request))

	

@login_required
def add_imgs(request,pk):
	imgpk = pk
	if request.method=="POST":
		img_form = AnImageForm1(request.POST, request.FILES)
		if img_form.is_valid():
			
			img1 = img_form.cleaned_data.get('img_1')
			img2 = img_form.cleaned_data.get('img_2')
			img3 = img_form.cleaned_data.get('img_3')
			img4 = img_form.cleaned_data.get('img_4')
			img5 = img_form.cleaned_data.get('img_5')

			cr_img1 = AnImage.objects.create(image = img1)
			cr_img2 = AnImage.objects.create(image = img2)
			cr_img3 = AnImage.objects.create(image = img3)
			cr_img4 = AnImage.objects.create(image = img4)
			cr_img5 = AnImage.objects.create(image = img5)

			new_list = AllowedList.objects.create(img_1=cr_img1, 
				img_2=cr_img2, 
				img_3=cr_img3, 
				img_4= cr_img4, 
				img_5=cr_img5)

			AdImg.objects.filter(pk=imgpk).update(imgs=new_list)

			return redirect('userpost_profilehome')

	else:
		img_form = AnImageForm1()

	args = {}
	args.update(csrf(request))

	args['img_form'] = AnImageForm1()
	args['adpk'] = imgpk

	return render_to_response('up_adimgs.html', args, context_instance=RequestContext(request))






@login_required
def user_mess(request):

	args = {}
	args.update(csrf(request))


	return render_to_response('up_mess.html', args, context_instance=RequestContext(request))
	



@login_required
def edit_profile(request):
	user_instance = User.objects.get(username=request.user)
	my_profile_instance = MyProfile.objects.get(user=user_instance)
	#avatar_instance = AnAvatar.objects.get(pk=1)

	if request.method == "POST":
		form1 = UserUpdateForm(request.POST, instance=user_instance)
		form2 = UpdateProfileForm(request.POST, instance=my_profile_instance)

		if form1.is_valid() and form2.is_valid():
			a =form1.save()
			b =form2.save(commit=False)
			#b.avatar = avatar_instance




			return redirect('userpost_edit_settings')
			

	else:
		form1 = UserUpdateForm(instance=user_instance)
		form2 = UpdateProfileForm(instance=my_profile_instance)



	
	avimg = my_profile_instance.avatar.image.thumbnail['100x100'].url
	args = {}
	args.update(csrf(request))
	args['form1'] = form1
	args['form2'] = form2
	args['avatarimg'] = avimg
	args['img'] = ImgUpdate
	
	



	return render(request, 'setting.html',args,context_instance=RequestContext(request))


	
@login_required
def delete_post(request,pk):
	delpk = pk

	AdImg.objects.filter(pk=delpk).delete()
	return redirect('userpost_profilehome')
	

@login_required
def delete_img(request,pk,po):
	an_image_instance = AnImage.objects.filter(id=pk)
	an_image_instance.update(image = "")
	
	return redirect('userpost_editpost',pk=po)


@login_required
def delete_ava(request,pk,po):
	an_image_instance = AnAvatar.objects.filter(id=pk)
	an_image_instance.update(image = "")
	
	return redirect('userpost_edit_settings',pk=po)

@login_required
def change_ava(request,pk):

	an_image_instance = AnAvatar.objects.filter(pk=pk)

	if request.method == "POST":
		imgform = ImgUpdate(request.FILES, request.POST)
		new_ava_instance = AnAvatar.objects.create(image=imgform)

		MyProfile.objects.filter(user=request.user).update(avatar=new_ava_instance)

		redirect('userpost_edit_settings')



@login_required
def update_image(request,pk,al,ot,po):
	#pk is image instance
	#po is adpost instance
	#al is allowed list instance
	#ot is the number to track for deletion
	oldpk = pk
	AllowedList_instance = AllowedList.objects.filter(pk=al)
	an_image_instance = AnImage.objects.filter(id=pk)
	if request.method == "POST":
		img_form = ImgUpdate(request.POST,request.FILES)
		if img_form.is_valid():
			newimg = img_form.cleaned_data.get('image')
			newimginstance = AnImage.objects.create(image=newimg)

			if ot == "1":
				AllowedList_instance.update(img_1=newimginstance)
				AnImage.objects.filter(id=oldpk).delete()
				return redirect('userpost_editpost', pk=po)
				
			elif ot =="2":
				AllowedList_instance.update(img_2=newimginstance)
				AnImage.objects.filter(id=oldpk).delete()
				return redirect('userpost_editpost', pk=po)


			elif ot == "3":
				AllowedList_instance.update(img_3=newimginstance)
				AnImage.objects.filter(id=oldpk).delete()
				return redirect('userpost_editpost', pk=po)
			elif ot == "4":
				AllowedList_instance.update(img_4=newimginstance)
				AnImage.objects.filter(id=oldpk).delete()
				return redirect('userpost_editpost', pk=po)
			elif ot == "5":
				AllowedList_instance.update(img_5=newimginstance)
				AnImage.objects.filter(id=oldpk).delete()
				return redirect('userpost_editpost', pk=po)

			else:
				return redirect('userpost_editpost', pk=po)


				
			return redirect('userpost_editpost', pk=po)

 
@login_required
def preview_ad(request,pk):

	pass

@login_required
def profile_progress(request):
	return render_to_response('up_progress.html', context_instance=RequestContext(request))


def alpha_home(request):
	return render_to_response('alpha_home.html', context_instance=RequestContext(request))