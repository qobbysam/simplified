
from django.conf.urls import  include, url

from userpost import views
urlpatterns = [
	url(r'^edit-profile/', views.edit_profile, name='userpost_edit_settings'),
	url(r'^home/', views.profile_home, name = 'userpost_profilehome'),
	url(r'^message/', views.user_mess, name = 'userpost_user_messages'),
	url(r'^create/', views.create_ad, name='userpost_createad'),
	url(r'^ad-imgs/(?P<pk>\d+)/$',views.add_imgs, name='userpost_addimgs'),
	url(r'^edit-post/(?P<pk>\d+)/$', views.edit_ad, name='userpost_editpost'),
	url(r'^preview/(?P<pk>\d+)/$', views.preview_ad, name='userpost_preview'),
	url(r'^profile', views.profile_progress, name='userpost_profile_progress'),
	url(r'^delete-post/(?P<pk>\d+)/$', views.delete_post, name='userpost_deletepost'),
	url(r'^delete-img/(?P<pk>\d+)/(?P<po>\d+)$', views.delete_img, name='userpost_deleteimg'),
	url(r'^update-img/(?P<pk>\d+)/(?P<al>\d+)/(?P<ot>\d+)/(?P<po>\d+)$', views.update_image, name='userpost_updateimg'),
	url(r'^alpha-home/', views.alpha_home)

]


''' urls for all applications that require login. '''